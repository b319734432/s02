function div_check(n){
	if(n % 5 == 0) return true;
	if(n % 7 == 0) return true;
	if(n % 5 !==0 || n % 7 !== 0) return false;
};

module.exports = {
	factorial: div_check
}