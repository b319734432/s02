const { factorial } = require('./util.js');
const { expect, assert } = require('chai');

it('test_factorial_0!_is_1', () => {
	const product = factorial(0);
	expect(product).to.equal(1);
});

it('test_factorial_4!_is_24', () => {
	const product = factorial(4);
	assert.equal(product, 24);
});

it('test_factorial_10!_is_3628800', () => {
	const product = factorial(10);
	expect(product).to.equal(3628800);
});